#include <LiquidCrystal.h>
#include <SPI.h>
#include <Ethernet.h>

//setting ethernet 
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
 
// Enter the IP address for Arduino, as mentioned we will use 192.168.0.16
// Be careful to use , insetead of . when you enter the address here
IPAddress ip(192,168,1,16);

// Set ip server
char server[] = "192.168.1.1"; 

// Initialize the Ethernet server library
EthernetClient client;

int trig = 2;
int echo = 3;
float durasi;
long jarak;
int H2, HT;
LiquidCrystal lcd(13, 12, 7, 6, 5, 4);

void setup(){
  // start the Ethernet connection
  Ethernet.begin(mac, ip);
  
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  Serial.begin(9600);
  HT = 200;
}

void loop(){
  lcd.begin(16, 2);

  digitalWrite(trig, LOW);
  delayMicroseconds(8);
  digitalWrite(trig, HIGH);
  delayMicroseconds(8);
  digitalWrite(trig, LOW);
  delayMicroseconds(8);

  jarak = pulseIn(echo, HIGH);
  jarak = jarak / 58;
  
  H2 = HT - jarak;
  Serial.print(H2);
  lcd.setCursor(0,0); 
  lcd.setCursor(0,0);
  lcd.print("Tinggi = ");
  lcd.print(H2);
  lcd.print(" CM");
  delay (2000);
  String ZZ=String(H2,DEC);
  if (client.connect(server, 80)) 
  {
     Serial.println("--> connection sukses\n");
     Serial.println(ZZ);
     client.print("GET /pengukur/rite.php?value="); // This
     client.print(ZZ); 
     client.println(" HTTP/1.1"); // Part of the GET request
     client.println("Host: 192.168.1.1 \r\n");
     Serial.println("Host OK");
     
     client.println( "Content-Type: application/x-www-form-urlencoded \r\n" );
     Serial.println("Content type OK"); 
     client.println("Connection: close"); //  Part of the GET request telling the server that we are over transmitting the message
     client.println(); // Empty line
     client.println(); // Empty line
     client.stop();    // Closing connection to server
  }
  else {
    // If Arduino can't connect to the server (your computer or web page)
    Serial.println("--> connection failed\n");
  }
  lcd.clear();
  delay (500);
}
